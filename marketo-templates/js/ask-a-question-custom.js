MktoForms2.whenReady(function(form) {
 //Dependant selections function
  var master = document.getElementById('mWebsiteInquiryType');
        var dependent = document.getElementById('mWebsiteTextFieldMarketingAlert');
  //select-dependencies are in a rich text area in the form in a JSON package, see here: https://nation.marketo.com/t5/Product-Discussions/Field-Values-Based-on-Previous-Selections/m-p/132392
 //custom visibility rules for the master and dependent -field values
        var dependencies = {
  "Prospect": {
   "Sales": true, 
   "Resource Center": true, 
   "General": true
  },
  "Client": {
    "Client Success": true, 
    "Support Page": true, 
    "Resource Center": true, 
    "General": true 
  },
  "User": {
    "School App Support": true,
    "Municipality App Support": true,
    "Billing Support": true, 
   "General": true
  },
  "Career": {
    "Careers": true, 
    "Current Openings": true, 
   "General": true
  }, 
  "Investor": {
    "Investor": true, 
    "Investor Resources": true, 
   "General": true
  }, 
  "Reporter": {
    "Media": true, 
    "Media Contact": true, 
   "General": true
  }
} ;
        var depId = dependent.id, templateId = depId + '_template';
        
        // create template shell
        var dynamicDependentTemplate = document.createElement('SELECT');
        dependent.parentNode.appendChild(dynamicDependentTemplate);        
        
        // clone initial select into template w/modified id
        dependent.setAttribute('id', templateId);
        dynamicDependentTemplate.outerHTML = dependent.outerHTML;
        
        // reset to original id, truncate to placeholder option ("Select...")
        dependent.setAttribute('id', depId);
        dependent.options.length = 1;
        
        // reset reference and eject template from DOM
        dynamicDependentTemplate = document.getElementById(templateId);
        dynamicDependentTemplate.parentNode.removeChild(dynamicDependentTemplate);
        
        // run through master's selected options on every change and repopulate dependent from template
        master[master.addEventListener ? 'addEventListener' : 'attachEvent'](
            master.addEventListener ? 'change' : 'onchange',function(){
            dependent.options.length = 1;
            
            for ( var opts = dynamicDependentTemplate.options, 
                    currentOpt = 1, 
                    maxOpt = opts.length; currentOpt < maxOpt; currentOpt++ ) 
            {
                var newOpt = new Option(), optValue = opts[currentOpt].getAttribute('value');    

                for ( var selects = master.options, 
                        currentSelect = 1, 
                        maxSelect = selects.length; currentSelect < maxSelect; currentSelect++ )               
                { 
                    if ( !selects[currentSelect].selected ) continue;
    
                    masterValue = selects[currentSelect].getAttribute('value');
                    if ( dependencies[masterValue] && optValue in dependencies[masterValue] ) {
                        dependent.appendChild(newOpt);
                        newOpt.innerHTML = opts[currentOpt].innerHTML; // Old IE can't use outerHTML
                        newOpt.setAttribute('value', optValue);
                    }
                }
            }
        });   
 
 //hide-show submit
 var formEl = form.getFormElem()[0],
        selectEl = formEl.querySelector("select[name='mWebsiteTextFieldMarketingAlert']"), // the SELECT w/interesting values
        showSubmitWhen = ["Client Success", "Sales", "Media Contact", "General"], // values for which to show Submit button
        submitRow = formEl.querySelector(".mktoButtonRow"); 
  //  the row w/the submit button
      
   selectEl.addEventListener("change",function(e){
      var submitShowable = showSubmitWhen.some(function(submittableValue){ 
         return selectEl.value == submittableValue; 
      });
  
      formEl.setAttribute("data-show-submit", submitShowable);
   });
 });