/**
 * @version v1 2020-04-04
 * @copyright © 2020 Sanford Whiteman
 * @license Hippocratic 2.1
 * https://blog.teknkl.com/using-a-mktoimg-element-as-a-css-background-image/
 */
(function(){
    var arrayify = getSelection.call.bind([].slice),
        srcSetters = {
          "data-background-for" : function(el,src){ el.style.backgroundImage = "url(\"" + src + "\")"; },
          "data-src-for" : function(el,src){ el.src = src; }
        };                  
  
    function bindImgBrowsers(){
      var imgBrowsers = document.querySelectorAll(".imgBrowserOnly");
  
      arrayify(imgBrowsers)
        .forEach(function(browseWrapper){
           var hasImgChild = !!browseWrapper.querySelector("img");
  
           if (hasImgChild) {
             pushSelected({currentTarget : browseWrapper});
           } else {
             browseWrapper.addEventListener("load", pushSelected, true);
           }
        });     
    }
  
    function pushSelected(e){
      var wrapperEl = e.currentTarget,
          selectedImgSrc = wrapperEl.querySelector("img").src;
  
      Object.keys(srcSetters)
        .forEach(function(setterName){
          if(wrapperEl.hasAttribute(setterName)){
            var targetEls = document.querySelectorAll(wrapperEl.getAttribute(setterName));
  
            arrayify(targetEls)
              .forEach(function(targetEl){
                srcSetters[setterName](targetEl,selectedImgSrc);
              });
          }
        });
    }
  
    document.addEventListener("DOMContentLoaded",bindImgBrowsers);
  })();