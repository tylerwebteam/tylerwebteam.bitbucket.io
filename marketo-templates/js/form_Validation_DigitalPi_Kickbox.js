/*!
 * AUTHOR: Jon Bourne, jonbourne.com
 * COMPANY: Digital Pi, digitalpi.com
 * REVISED: 2019-08-16
 */
;(function(){ 'use strict';

    var errorMsgs = {
        format_invalid : 'Email format is invalid. Please enter a valid email address.',
        disposable :     'Email is disposable. Please enter a valid email address.'
    };

    var autoSubmitted = false;

    var version = 190816;

    var _ = function(msg) {
        if(location.hash === '#debug') {
            console.log(msg);
        }
    };

    var emailFormatValid = function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    var init = function() {
        MktoForms2.whenReady( function(form) {
            var $ = MktoForms2.$;
            form.onValidate(function(isValid){
                if(!isValid) return;
                if(!autoSubmitted) {
                    form.submittable(false);
                    _('Checking whether email format is valid for '+form.vals().Email+'...');
                    if(emailFormatValid(form.vals().Email)) {
                        _('Email format is valid');
                        _('Checking whether email address '+form.vals().Email+' is disposable...');
                        $.ajax({
                            dataType:'json',
                            url:'https://open.kickbox.com/v1/disposable/'+form.vals().Email.split('@')[1],
                            success:function(json){
                                if(Object.keys(json).includes('disposable') && json.disposable) {
                                    _(form.vals().Email+' is a disposable address; disallow and show error');
                                    form.showErrorMessage(errorMsgs.disposable,$('#Email'));
                                }
                                else {
                                    _('Kickbox confirmed that email address is not disposable; allow form submission');
                                    form.submittable(true);
                                    autoSubmitted = true;
                                    form.submit();
                                }
                            },
                            error:function(error){
                                _('Error contacting Kickbox service; allowing form submission');
                                console.error('Could not contact Kickbox to validate email address '+form.vals().Email);
                                form.submittable(true);
                                autoSubmitted = true;
                                form.submit();
                            }
                        });
                    }
                    else {
                        _('Email format is not valid; disallow and show error');
                        form.showErrorMessage(errorMsgs.format_invalid,$('#Email'));
                    }
                }
            });
        });
    };

    var c = function() {
        var b = '%cBetter Email Validation for %cMarketo. %c❤%c, @digitalpi. %cVersion: '+version+'. Append #debug to URL to enable console logging.';
        var p = {
            1:"WFhYWCAgIFhYWFhYWFhYICAgIFhYWFg=",2:"ICAgIFhYWA==",
            3:"WFhYWCAgICAgICBYWFhYICAgIFhYWFg=",4:"ICAgIFhYWFhY",
            5:"WFhYWFhYWFhYWFhYWFhYWFhYWFhYWFg=",6:"ICAgIFg=",
            7:"WFhYWCAgIFhYICAgWFhYICAgIFhYWFg="
        };
        var lap = [5,5,3,7,7,3,1,1,5,5,4,2,6];
        var la = '';
        lap.forEach(function(e){
            la+=atob(p[e])+"\n";
        });
        console.log( b, 'font-weight:bold;color:#646669', 'font-weight:bold;color:#5944b0',
            'font-size:125%;color:#3AA5B6', 'color:#3AA5B6', 'font-weight:normal;color:#C4C6C9'
            );
        if(location.hash === '#debug') {
            console.log(
                '%c'+la+"%cBetter Email Validation for Marketo\n%cAuthor: Jon Bourne, jonbourne.com\nCompany: Digital Pi, digitalpi.com\nVersion: "+version
                +"\nFree disposable email validation courtesy of kickbox.com",
                'color:#3AA5B6', 'font-weight:bold;color:#3AA5B6', 'color:#C4C6C9'
            );
        }
    };

    if(/complete|interactive|loaded/.test(document.readyState)) { c(); init(); }
    else { document.addEventListener('DOMContentLoaded', function() { c(); init(); }); }
})();
