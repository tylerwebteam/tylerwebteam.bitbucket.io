$("#results").before($("#urlouttext"));
//functions to show form field values in the results element
function showValues() {
    var str = $("form").serialize(); //serialize any form data with a name attribute
    var strArray = $("form").serializeArray(); //create array for injection into div
    var urlout = $("#url").val(); //get the url from the field
    var results = $("#results");
    var resultsOutput = $("#resultsOutput");
    var resultsTerms = $("#resultsTerms");
    var resultsContent = $("#resultsContent");
    var dataObj = {}; //used to inject values into results div
    //strip out Array values for all utms
    for (i = 0; i < strArray.length; i++) {
        dataObj[strArray[i].name] = strArray[i].value;
    }
    //console.log(str); 
    console.log(dataObj);
    console.log(results);
    //results.text(str); 
    // inject final url output to results element
    //removing spaces with .replace(/\s/g, '') 
    results.text(urlout.replace(/\s/g, '') + "?" + "utm_source=" + dataObj['utm_source'] + "&" + "utm_medium=" + dataObj['utm_medium'] + "&" + "utm_campaign=" + dataObj['utm_campaign'].replace(/\s/g, ''));
    if (dataObj['utm_term'] !== "") {
        resultsTerms.text("&" + "utm_term=" + dataObj['utm_term'].replace(/\s/g, '')); // inject final url output to results element
    }
    if (dataObj['utm_content'] !== "") {
        resultsContent.text("&" + "utm_content=" + dataObj['utm_content'].replace(/\s/g, ''));
    }
}
showValues();
//function to copy the url element to the users clipboard via a button
function copyDivToClipboard() {

    //function not copying all three results spans, need to find a way to copy all three. creating 3 ranges did not work. need to find way to concatenate all three spans together first, then have selectNode use that element for copy
    var range = document.createRange();
    console.log(range);
    range.selectNodeContents(document.getElementById("resultsOutput"));
    window.getSelection().removeAllRanges(); // clear current selection
    window.getSelection().addRange(range); // to select text
    document.execCommand("copy");
    window.getSelection().removeAllRanges(); // to deselect
}

// JavaScript for builder validation on required fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            //validation fires if user leaves a require field blank using blur
            form.addEventListener('blur', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, true);
        });
    }, false);
})();
//JS to post the builder results to a Google Sheet

var request; // Variable to hold request

// Bind to the submit event of our form
$("#utm_builder").submit(function (event) {

    // Abort any pending request
    if (request) {
        request.abort();
    }
    // setup some local variables
    var $form = $(this);

    // Let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");

    // Serialize the data in the form
    var serializedData = $form.serialize();

    // Let's disable the inputs for the duration of the Ajax request.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    $inputs.prop("disabled", true);

    // Fire off the request to /form.php
    request = $.ajax({
        url: "https://script.google.com/macros/s/AKfycbzDc0cjs64gMFxDZBE-y2DgJdF5JC_ejgmnAcBq/exec",
        type: "post",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
        // Log a message to the console
        console.log("Hooray, it worked!");
        console.log(response);
        console.log(textStatus);
        console.log(jqXHR);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        // Log the error to the console
        console.error(
            "The following error occurred: " +
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });

    // Prevent default posting of form
    event.preventDefault();
});